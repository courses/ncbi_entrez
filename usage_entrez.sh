# dans un terminal bash

## search Rana temporaria entreies in "Nucleotide" database from NCBI
## and then keep only entries with the "complete genome" mention
esearch -db Nucleotide -query "rana temporaria mitochondrion" | efilter -query "complete genome"
## download reference Rana temporaria mitochondrion FASTA file
efetch -format fasta -id NC_042226.1 -db Nucleotide > mitogenome/NC_042226.1.fas
