# ncbi_entrez

Understand the NCBI database and to do request using `entrez` and `Rentrez`

# Rentrez

Install `rentrez` R package

```
install.packages("rentrez")
library(rentrez)
```

https://cran.r-project.org/web/packages/rentrez/vignettes/rentrez_tutorial.html
