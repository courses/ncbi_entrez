library(rentrez)


## search Rana temporaria entreies in "Nucleotide" database from NCBI
## search returns a list of IDs
## retmax argument is to ask more than 20 (default value) results
query <- entrez_search(db="nucleotide", term="rana temporaria[ORGN] AND mitochondrion AND complete genome", retmax=99999)
## see the IDs of the queries
query$ids
## item i can search in nucleotide database
entrez_db_searchable('nucleotide')
## search the gene named "mhc" and species from "actinopterygii" class organism
query <- entrez_search(db="nucleotide",
                       term= "(MHC[GENE] OR (major histocompatibility
complex[MeSH])) AND actinopterygii[ORGN] AND 1992:2021[PDAT]",
                       retmax=99999,
                       use_history=TRUE)

search_year <- function(year, term){
  query <- paste(term, " AND (", year, "[PDAT])")
  entrez_search(db="nucleotide", term=query, retmax=0)$count
}


year <- 1992:2021
publications <- sapply(year,
                 search_year,
                 term="(MHC[GENE] OR (major histocompatibility complex[MeSH])) AND actinopterygii[ORGN]",
                 USE.NAMES=FALSE)

plot(year, publications, type="l",  main="MHC actinopterygii")

recs <- entrez_fetch(db="nucleotide", id=query$ids[1:30], rettype="fasta")
write(recs, file="query.fasta")

## upload large queries

for( seq_start in seq(1,length(query$ids),50)){
  recs <- entrez_fetch(db="nucleotide", web_history=query$web_history,
                       rettype="fasta", retmax=50, retstart=seq_start)
  cat(recs, file="query.fasta", append=TRUE)
  cat(seq_start+49, "sequences downloaded\r")
}




### seek MHC gene
entrez_db_summary("gene")
entrez_db_searchable("gene")

query <- entrez_search(db="gene", term= "(MHC II DRB[GENE]) OR (major histocompatibility complex[MeSH])")

link <- entrez_link(db="nucleotide", dbfrom="gene", id=query[1])

nuclinks <- link$links$gene_nuccore

recs <- entrez_fetch(db="nucleotide", id=nuclinks[1], rettype="fasta")

